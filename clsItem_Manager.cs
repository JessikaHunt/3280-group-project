﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CS3280GroupProject
{
    class clsItem_Manager
    {

        clsData db;
        DataSet ds;

        /// <summary>
        /// New instance 
        /// </summary>
        public clsItem_Manager()
        {
            db = new clsData();

        }

        /// <summary>
        /// List items
        /// </summary>
        /// <returns></returns>
        public List<clsItemDetails> Items()
        {
            List<clsItemDetails> lstItems = new List<clsItemDetails>();
            string sSQL = clsSQL.getItems();

            int iRet = 0;

            ds = db.ExecuteSQLStatement(sSQL, ref iRet);

            for (int i = 0; i < iRet; i++)
            {
                clsItemDetails Itemreturn = new clsItemDetails();
                Itemreturn.sItemCode = ds.Tables[0].Rows[i][0].ToString();
                Itemreturn.sItemDesc = ds.Tables[0].Rows[i][1].ToString();
                Itemreturn.sCost = ds.Tables[0].Rows[i][2].ToString();
                lstItems.Add(Itemreturn);
            }
            return lstItems;
        }

        /// <summary>
        /// Add invoice to data base
        /// </summary>
        public void addToDB()
        {
            string sSQL = clsSQL.addInvoice();

            int iRet = 0;

            iRet = db.ExecuteNonQuery(sSQL);

        }

        /// <summary>
        /// Class of items
        /// </summary>
        public class clsItemDetails
        {
            public string sItemCode { get; set; }
            public string sItemDesc { get; set; }
            public string sCost { get; set; }


             public override string ToString()
              {
                  return sItemCode + " - " + sItemDesc + " - $" + sCost;
             }
        }

        public void addItemto()
        {

        }
    }
}
