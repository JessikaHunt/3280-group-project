﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CS3280GroupProject
{
    class clsInvoice_Manager
    {
        clsData db;
 

        DataSet ds;

        public clsInvoice_Manager()
        {
            db = new clsData();

        }

        public List<clsInvoiceDetails> Invoices()
        {
            List<clsInvoiceDetails> lstInvoices = new List<clsInvoiceDetails>();
            string sSQL = clsSQL.getInvoicesmine();

            int iRet = 0;

            ds = db.ExecuteSQLStatement(sSQL, ref iRet);

            for (int i = 0; i < iRet; i++)
            {
                clsInvoiceDetails Invoicereturn = new clsInvoiceDetails();
                Invoicereturn.sInvoiceNum = ds.Tables[0].Rows[i][0].ToString();
                Invoicereturn.sInvoiceDate = ds.Tables[0].Rows[i][1].ToString();
                Invoicereturn.sTotalCharge = ds.Tables[0].Rows[i][2].ToString();
                lstInvoices.Add(Invoicereturn);
            }
            return lstInvoices;


        }

        public void addInvoice()
        {
            string sSQL = clsSQL.addInvoice();
            int iRet = 0;

            ds = db.ExecuteSQLStatement(sSQL, ref iRet);

        }

        public class clsInvoiceDetails
        {
            public string sInvoiceNum { get; set; }
            public string sInvoiceDate { get; set; }
            public string sTotalCharge { get; set; }




           // public override string ToString()
          //  {
          //      return sFlight_Number + " - " + sAirCraft_Type;
           // }
        }

    }
}
