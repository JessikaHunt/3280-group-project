﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Data;


namespace CS3280GroupProject
{
    /// <summary>
    /// Interaction logic for winSearch.xaml
    /// </summary>
    public partial class winSearch : Window
    {
        /// <summary>
        /// the SQL class
        /// </summary>
        clsSQL SQL = new clsSQL();
        /// <summary>
        /// DataSet to fill the dataGrid
        /// </summary>
        private DataSet ds = new DataSet();
        /// <summary>
        /// String for the invoice Search
        /// </summary>
        private string sInvoiceSearch;
        /// <summary>
        /// string for the amount search
        /// </summary>
        private string sAmountSearch;
        /// <summary>
        /// string for the date search
        /// </summary>
        private string sDateSearch;

        public winSearch()
        {
            InitializeComponent();
            SQL.selectAllInvoices();
            combo_InvoiceID.ItemsSource = SQL.getInvoices();
            combo_Date.ItemsSource = SQL.getDates();
            combo_Amount.ItemsSource = SQL.getAmounts();
            ds = SQL.getDataSet();
            dg_invoice.ItemsSource = ds.Tables[0].DefaultView;
        }

        private void btn_Search_Click(object sender, RoutedEventArgs e)
        {
            sInvoiceSearch = combo_InvoiceID.SelectionBoxItem.ToString();
            sAmountSearch = combo_Amount.SelectionBoxItem.ToString();
            sDateSearch = combo_Date.SelectionBoxItem.ToString();
            SQL.searchInvoices(sInvoiceSearch, sAmountSearch, sDateSearch);
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
