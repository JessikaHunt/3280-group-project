﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Collections.ObjectModel;

namespace CS3280GroupProject
{
    /// <summary>
    /// Interaction logic for addInvoice.xaml
    /// </summary>
    public partial class addInvoice : Window
    {
        private clsInvoice_Manager clsInvoice;
        private clsItem_Manager clsItems;
        ObservableCollection<clsItem_Manager.clsItemDetails> lstPeople1;

        /// <summary>
        /// Main window form
        /// </summary>
        public addInvoice()
        {
            InitializeComponent();
            clsInvoice = new clsInvoice_Manager();
            clsItems = new clsItem_Manager();

            comboBox.ItemsSource = clsItems.Items();
            lstPeople1 = new ObservableCollection<clsItem_Manager.clsItemDetails> { };
            dgInventoryList.ItemsSource = lstPeople1;
        }

        /// <summary>
        /// Button metod
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Click(object sender, RoutedEventArgs e)
        {
            addInvoicees();

        }

        /// <summary>
        /// Add invoice method
        /// </summary>
        private void addInvoicees()
        {
            string i = txtDate.Text;
            clsItems.addToDB();
        }

        private void btnAddItem_Click(object sender, RoutedEventArgs e)
        {
            clsItem_Manager.clsItemDetails Person;
            Person = (clsItem_Manager.clsItemDetails)comboBox.SelectedValue;
            dataGrid.ItemsSource = lstPeople1;
        }
    }
}
