﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CS3280GroupProject
{
    class clsSQL
    {
        /// <summary>
        /// Object to access the class to query the database
        /// </summary>
        private clsData clsData = new clsData();
        /// <summary>
        /// Dataset to hold the invoice table.
        /// </summary>
        private DataSet ds = new DataSet();
        /// <summary>
        /// List that holds all of the results for the invoice ID SQL script for the invoice combo box
        /// </summary>
        private List<string> invoiceList = new List<string>();
        /// <summary>
        /// List that holds all of the dates for the date combo box
        /// </summary>
        private List<string> dateList = new List<string>();
        /// <summary>
        /// List that holds all of the amounts for the amount combo box
        /// </summary>
        private List<string> amountList = new List<string>();
        /// <summary>
        /// SQL string that gets set in the different methods
        /// </summary>
        private string sSQL;
        /// <summary>
        /// variable to keep track of the rows that are returned from the query
        /// </summary>
        int iRows;
        /// <summary>
        /// variable to hold the invoices
        /// </summary>
        string sInvoices;
        /// <summary>
        /// Variable to hold the amounts
        /// </summary>
        string sAmounts;
        /// <summary>
        /// variable to hold the dates
        /// </summary>
        string sDates;
        /// <summary>
        /// List to be returned to the form for the dataGrid
        /// </summary>
        List<string> dataGridList = new List<string>();
        /// <summary>
        /// Value to be added to the dataGridList
        /// </summary>
        string sVal;

        /// <summary>
        /// Method gets the data based on the invoices
        /// </summary>
        /// <returns></returns>
        public static string getInvoicesmine()
        {

            string sSQL;
            sSQL = "SELECT InvoiceNum, InvoiceDate, TotalCharge FROM Invoices";
            return sSQL;
            
        }

        /// <summary>
        /// Method to add items back to database invoice
        /// </summary>
        /// <returns></returns>
        public static string addInvoice()
        {
            string sSQL;
            sSQL = "INSERT INTO Invoices (InvoiceDate, TotalCharge) Values (#4/8/2016#, 33.44);";
            return sSQL;
        }

        /// <summary>
        /// Get items method
        /// </summary>
        /// <returns></returns>
        public static string getItems()
        {
            string sSQL;
            sSQL = "SELECT ItemCode, ItemDesc, Cost FROM ItemDesc ORDER BY ItemCode;";
            return sSQL;
        }







        public string statement;

        /// <summary>
        /// This method takes an item's initial code (initCode), and updates the code,
        /// description, and cost to the values passed in: newCode, desc, and cost, respectively.
        /// </summary>
        /// <param name="initCode"></param>
        /// <param name="newCode"></param>
        /// <param name="desc"></param>
        /// <param name="cost"></param>
        public string updateInventoryItem(string initCode, string newCode, string desc, float cost)
        {
            statement = "UPDATE ItemDesc SET ItemCode = '" + newCode + "', ItemDesc = '" + desc +
                "', Cost = '" + cost + "' WHERE ItemCode = '" + initCode + "';";
            return statement;
        }

        /// <summary>
        /// This method takes the invoice number (string sInvoiceNum) and the item code (sItemCode)
        /// and adds the item to the specified invoice.
        /// <param name="sInvoiceNum"></param>
        /// <param name="sItemCode"></param>
        /// </summary>
        public void addItemToInvoice(string sInvoiceNum, string sItemCode)
        {

        }

        /// <summary>
        /// The addItemToInvoice adds a specific item to a specified invoice.  The method takes
        /// in the invoice number (invoiceNum) and the item code (itemCode) to add to the invoice.
        /// The method sends a SQL statement to update the LineItems table in the database.
        /// </summary>
        /// <param name="invoiceNum"></param>
        /// <param name="lineItemNum"></param>
        /// <param name="itemCode"></param>
        public void addItemToInvoice(int invoiceNum, int lineItemNum, char itemCode)
        {

        }

        /// <summary>
        /// The deleteItemFromInvoice method removes a specified item from a specified invoice.
        /// It accepts the invoice number (invoiceNum), the item to be removed (itemCode), and the
        /// line number.  The method sends a SQL statement to delete the record in the 
        /// LineItems table in the database.
        /// </summary>
        /// <param name="invoiceNum"></param>
        /// <param name="lineItemNum"></param>
        /// <param name="itemCode"></param>
        public void deleteItemFromInvoice(int invoiceNum, int lineItemNum, char itemCode)
        {

        }

        public void createItem()
        {

        }

        public void createInvoice()
        {

        }

        /// <summary>
		/// This method selects all invoices from the database. 
		/// </summary>
		public void selectAllInvoices()
        {
            sSQL = "SELECT InvoiceNum, InvoiceDate,TotalCharge FROM INVOICES";
            ds = clsData.ExecuteSQLStatement(sSQL, ref iRows);
            setLists();
        }
        /// <summary>
        /// This method selects the specific invoice that was chosen from combo_InvoiceID
        /// </summary>
        /// <param name="sInvoiceID"></param>
        public void selectInvoiceID(string sInvoiceID)
        {
            sSQL = "SELECT InvoiceNum, InvoiceDate, TotalCharge FROM INVOICES WHERE InvoiceNum = " + sInvoiceID;
            ds = clsData.ExecuteSQLStatement(sSQL, ref iRows);
            setLists();
        }
        /// <summary>
        /// Selects all invoices by the Date that is chosen from combo_Date
        /// </summary>
        /// <param name="sDate"></param>
        public void selectInvoiceByDate(string sDate)
        {
            sSQL = "SELECT InvoiceNum, InvoiceDate, TotalCharge FROM INVOICES WHERE InvoiceDate = " + sDate;
            ds = clsData.ExecuteSQLStatement(sSQL, ref iRows);
            setLists();
        }
        /// <summary>
        /// Selects the invoices that match the amount chosen from combo_Amount
        /// </summary>
        /// <param name="sAmount"></param>
        public void selectInvoiceByAmount(string sAmount)
        {
            sSQL = "SELECT InvoiceNum, InvoiceDate, TotalCharge FROM INVOICES WHERE TotalCharge = " + sAmount;
            ds = clsData.ExecuteSQLStatement(sSQL, ref iRows);
            setLists();
        }
        /// <summary>
        /// this creates all of the lists
        /// </summary>
        private void setLists()
        {
            for (int i = 0; i < iRows; i++)
            {
                sInvoices = ds.Tables[0].Rows[i][0].ToString();
                sDates = ds.Tables[0].Rows[i][1].ToString();
                sAmounts = ds.Tables[0].Rows[i][2].ToString();

                invoiceList.Add(sInvoices);
                dateList.Add(sDates);
                amountList.Add(sAmounts);
            }
        }
        /// <summary>
        /// This returns the invoice list
        /// </summary>
        /// <returns></returns>
        public List<string> getInvoices()
        {
            return invoiceList;
        }
        /// <summary>
        /// this returns the amount list
        /// </summary>
        /// <returns></returns>
        public List<string> getAmounts()
        {
            return amountList;
        }
        /// <summary>
        /// returns the date list
        /// </summary>
        /// <returns></returns>
        public List<string> getDates()
        {
            return dateList;
        }
        /// <summary>
        /// returns the dataSet to update the Grid
        /// </summary>
        /// <returns></returns>
        public DataSet getDataSet()
        {
            return ds;
        }
        public void searchInvoices(string sInvoice, string sAmount, string sDate)
        {

        }





    }
}
