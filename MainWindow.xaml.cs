﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Collections.ObjectModel;

namespace CS3280GroupProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        /// <summary>
        /// An ObservableCollection to hold clsItem_Manager objects.  An ObservableCollection is a list that 
        /// provides notifications when items get added, removed, or the whole list is refreshed.
        /// </summary>
        ObservableCollection<clsItem_Manager.clsItemDetails> inventory;

        /// <summary>
        /// An ObservableCollection to hold clsInvoiceDetails objects.
        /// </summary>
        ObservableCollection<clsInvoice_Manager.clsInvoiceDetails> invoices;

        /// <summary>
        /// The data access, database connection and sql execution class.
        /// </summary>
        clsData dataAccess;

        addInvoice addInvoice;
        updateInventory updateInventory;
        updateInvoice updateInvoice;
        winSearch searchInvoices;

        /// <summary>
        /// Entry point to the application.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();


            //Populate the ObservableCollections inventory and invoices with
            //data from the database.  Right now I am hard coding one item to 
            //test the binding.
            //clsInvoice_Manager.clsInvoiceDetails inv1 = new clsInvoice_Manager.clsInvoiceDetails("1", "May 4, 2016", "16.50");
            //invoices.Add(inv1);
            //Again, hard coding one item to test the binding.
            //clsItem_Manager.clsItemDetails necklace = new clsItem_Manager.clsItemDetails("A", "Necklace", "12.00");
            //inventory.Add(necklace);
            //Initialize the data access
            dataAccess = new clsData();
        }

        /// <summary>
        /// The Create Invoice click event hides the main menu, shows the create invoice 
        /// window, and waits for the user to complete the activities on the create invoice 
        /// window.  Once the create invoice window is closed, the main menu shows.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateInvoice_Click(object sender, RoutedEventArgs e)
        {
            //Hide the Main Menu
            addInvoice = new addInvoice();
            //Launch the Create Invoice Window - this will be a ShowDialogue
            addInvoice.ShowDialog();
            //When the Create Invoice window exits, show the main menu again
            this.Show();
        }

        /// <summary>
        /// The Create Invoice click event hides the main menu, shows the create invoice 
        /// window, and waits for the user to complete the activities on the create invoice 
        /// window.  Once the create invoice window is closed, the main menu shows.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearchInvoices_Click(object sender, RoutedEventArgs e)
        {
            //Hide the Main Menu
            searchInvoices = new winSearch();
            //Launch the Search Invoices Window - this will be a ShowDialogue
            searchInvoices.ShowDialog();
            //When the Search Invoices window exits, show the main menu again
            this.Show();
        }

        /// <summary>
        /// The Create Invoice click event hides the main menu, shows the create invoice 
        /// window, and waits for the user to complete the activities on the create invoice 
        /// window.  Once the create invoice window is closed, the main menu shows.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteInvoice_Click(object sender, RoutedEventArgs e)
        {
            //Hide the Main Menu
            //Launch the Delete Invoice Window - this will be a ShowDialogue
            //winDeleteInvoice.ShowDialogue();
            //When the Delete Invoice window exits, show the main menu again
            this.Show();
        }

        /// <summary>
        /// The Create Invoice click event hides the main menu, shows the create invoice 
        /// window, and waits for the user to complete the activities on the create invoice 
        /// window.  Once the create invoice window is closed, the main menu shows.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdateInventory_Click(object sender, RoutedEventArgs e)
        {
            //Hide the Main Menu
            updateInventory = new updateInventory();
            //Launch the Update Inventory Window - this will be a ShowDialogue
            updateInventory.ShowDialog();            
            //When the Update Inventory window exits, show the main menu again
            this.Show();
        }

        /// <summary>
        /// The Create Invoice click event hides the main menu, shows the create invoice 
        /// window, and waits for the user to complete the activities on the create invoice 
        /// window.  Once the create invoice window is closed, the main menu shows.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearchInventory_Click(object sender, RoutedEventArgs e)
        {
            //Hide the Main Menu
            //Launch the Search Inventory Window - this will be a ShowDialogue
            //winSearchInventory.ShowDialogue();
            //When the Search Inventory window exits, show the main menu again
            this.Show();
        }

        private void btnUpdateInvoice_Click(object sender, RoutedEventArgs e)
        {
            //Hide the Main Menu
            updateInvoice = new updateInvoice();
            //Launch the Search Inventory Window - this will be a ShowDialogue
            updateInvoice.ShowDialog();
            //When the Search Inventory window exits, show the main menu again
            this.Show();
        }
    }

}
